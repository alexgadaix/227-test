#227 Test

A Bootstrap 4.0 (Beta 3) boiler plate with sass, concatenation, minification, autoprefixer, Browsersync, hot reloading and sourcemaps all runned by Gulp.

##Requirements
This project requires you to have a installation of nodejs with npm This project also requires you to have global installations of gulp.

Install gulp globally:

npm install -g gulp

##Gulp commands

###gulp serve

The gulp serve command starts a local Browsersync server that serves your files in the browser. It automatically reloads the current page when changing html, sass and js files. The output of all sass files go to main.css. All js files are concatenated into main.js. You can access the project live in development with other devices on the same network. Go to the “External” address specified by Browsersync in the terminal in the browser of your device.

gulp serve

###gulp (build)

The default gulp command is set to creating a “docs” directory with a production version of the project, ready to be deployed. It minifies and renames js/css assets as well as cleaning the old “docs” directory. CSS is auto prefixed for the latest two browser versions.

gulp

###gulp concatScripts

The gulp concatScripts command combines the specified js resources into main.js. You can add new js files to this command on line 16 in gulpfile.js You might want to run concatScripts once separately after adding new js files.

gulp concatScripts

